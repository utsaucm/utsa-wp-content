<?php
/**
* Template Name: Frontpage
*/
$utsamu_prefix = MU_PREFIX . 'slider_';

get_header();
?>


<?php
$postTypeName = MU_PREFIX . 'slider';
$args = array( 'post_type'=>$postTypeName, 'orderby' => 'menu_order','order' => 'ASC' );
$sliders = get_posts( $args );
$total_sliders = count($sliders);
?>
<section id="slide-section" class="slidesContainer">
  <div id="slides">
  <?php foreach ($sliders as $key => $slider) {
    $full_image_src = wp_get_attachment_image_src( get_post_meta( $slider->ID, $utsamu_prefix . 'background_image_id', true ), 'full' )[0];
    $slider_position    =   get_post_meta($slider->ID, $utsamu_prefix . 'position', true );
    $boxed              =   (get_post_meta($slider->ID, $utsamu_prefix . 'boxed', true )=='yes') ? 'boxed' : '';
    $has_button         =   (get_post_meta($slider->ID, $utsamu_prefix . 'button_text', true )=='') ? false : true;
    $button             =   get_post_meta($slider->ID, $utsamu_prefix . 'button_text', true );
    $button_url         =   get_post_meta($slider->ID, $utsamu_prefix . 'button_url', true );
    $video_url          =   get_post_meta($slider->ID, $utsamu_prefix . 'video_link', true );
    $video_type         =   get_post_meta($slider->ID, $utsamu_prefix . 'video_type', true );
    $bg_image_url       =   get_post_meta($slider->ID, $utsamu_prefix . 'background_image', true );
    $background_image   =   'background-image: url('. $full_image_src . ')';
    $columns            =   false;
    $embed_code = '';

    if( !empty($image_url) or !empty($video_url) ){
        $columns        =   true;
    }
    if( $video_type=='youtube' ) {
        $embed_code = '<iframe width="640" height="480" src="//www.youtube.com/embed/' . get_video_ID( $video_url ) . '?rel=0" frameborder="0" allowfullscreen></iframe>';
    } elseif( $video_type=='vimeo' ) {
        $embed_code = '<iframe src="//player.vimeo.com/video/' . get_video_ID( $video_url ) . '?title=0&amp;byline=0&amp;portrait=0&amp;color=a22c2f" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>';
    } else {
      $embed_code = '';
    }
  ?>
    <div class="slide container-fluid">
        <div class="slide-image">
          <img alt="alt text goes here"
            src="<?php echo $full_image_src;?>" />
        </div>

        <div class="slideContainer">
          <div class="slideRow">
            <div class="internalContiner">
              <div class="row">
                <div class="col-12 <?php echo ($columns) ? 'col-md-6' : 'col-md-12'  ?>">
                    <div class="mx-auto text-center">
                        <h2 class="<?php echo $boxed ?> animation animated-item-1 slideContentMainTitle">
                          <?php echo $slider->post_title ?>
                        </h2>
                        <p class="<?php echo $boxed ?> slideContentIntro animation animated-item-2">
                            <?php echo do_shortcode( $slider->post_content ) ?>
                        </p>
                        <?php if( $has_button ){ ?>
                        <br>
                        <a class="btn btn-lg orange whiteCTA text-xs-center animation animated-item-3"
                          href="<?php echo $button_url ?>"><?php echo $button ?></a>
                        <?php } ?>
                    </div>
                </div>

                <?php if($columns){ ?>
                <div class="col-sm-6 d-none d-md-inline hidden-xs animation animated-item-4">
                    <div class="mx-auto text-center">
                        <div class="embed-container">
                            <?php echo $embed_code; ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
    </div>

  <?php } ?>
  </div>
</section>

<section id="landing-intro" class="content">
  <div class="container">
    <div class="row"><div class="col-12">
      <h1><?php the_title(); ?></h1>
    </div></div>

    <div class="row"><div class="col-12">
    <?php the_post(); ?>
    <?php the_content(); ?>
  </div></div></div>
</section>
<?php
    get_footer();
