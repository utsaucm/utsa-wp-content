<?php
/*
* Theme Option Functions
*/

//Favicon Image

if (!function_exists("utsamu_favicon")) {
  function utsamu_favicon(){
    echo '<link rel="shortcut icon" href="' . get_template_directory_uri() . '/favicon.ico" />';
  }
  add_action('wp_head', 'utsamu_favicon');
}


function get_video_ID($link){
    if( empty($link) ) {
      return false;
    }
    $path  =  trim(parse_url($link, PHP_URL_PATH), '/');
    $query_string = parse_url($link, PHP_URL_QUERY);
    parse_str($query_string, $output);
    if( empty($output) ){
        return $path;
    } else {
        return $output['v'];
    }
}
