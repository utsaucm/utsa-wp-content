<?php
/**
 * Custom pagination functions this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package _utsamu
 */

 if( ! function_exists('zee_pagination') ){

/**
 * Display pagination
 * @return [string] [pagination]
 */
function zee_pagination() {
    global $wp_query;
    if ($wp_query->max_num_pages > 1) {
            $big = 999999999; // need an unlikely integer
            $items =  paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'prev_next'    => true,
                'current' => max( 1, get_query_var('paged') ),
                'total' => $wp_query->max_num_pages,
                'type'=>'array'
                ) );

            $pagination ="<ul class='pagination'>\n\t<li>";
            $pagination .=join("</li>\n\t<li>", $items);
            $pagination ."</li>\n</ul>\n";

            return $pagination;
        }
        return;
    }

}


if ( ! function_exists( 'zee_post_nav' ) ) {


/**
 * Display post nav
 * @return [type] [description]
 */

function zee_post_nav() {
    global $post;

    // Don't print empty markup if there's nowhere to navigate.
    $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
    $next     = get_adjacent_post( false, '', false );

    if ( ! $next and ! $previous ){
        return;
    }
    ?>
    <nav class="navigation post-navigation" role="navigation">
        <div class="pagination">
            <?php if ( $previous ) { ?>
            <li class="previous">
                <?php previous_post_link( '%link', _x( '<i class="icon-long-arrow-left"></i> %title', 'Previous post link', UTSATEXTDOMAIN ) ); ?>
            </li>
            <?php } ?>
            <li >
                <a href="<?php echo get_home_url(); ?>/faculty-staff">Back to Directory </a>
            </li>
            <?php if ( $next ) { ?>
            <li class="next"><?php next_post_link( '%link', _x( '%title <i class="icon-long-arrow-right"></i>', 'Next post link', UTSATEXTDOMAIN ) ); ?></li>
            <?php } ?>

        </div><!-- .nav-links -->
    </nav><!-- .navigation -->
    <?php
}
}


if( ! function_exists('zee_link_pages') ){

    function zee_link_pages($args = '') {
        $defaults = array(
            'before' => '' ,
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'next_or_number' => 'number',
            'nextpagelink' => __('Next page', UTSATEXTDOMAIN),
            'previouspagelink' => __('Previous page', UTSATEXTDOMAIN),
            'pagelink' => '%',
            'echo' => 1
            );

        $r = wp_parse_args( $args, $defaults );
        $r = apply_filters( 'wp_link_pages_args', $r );
        extract( $r, EXTR_SKIP );

        global $page, $numpages, $multipage, $more, $pagenow;

        $output = '';
        if ( $multipage ) {
            if ( 'number' == $next_or_number ) {
                $output .= $before . '<ul class="pagination">';
                $laquo = $page == 1 ? 'class="disabled"' : '';
                $output .= '<li ' . $laquo .'>' . _wp_link_page($page -1) . '&laquo;</li>';
                for ( $i = 1; $i < ($numpages+1); $i = $i + 1 ) {
                    $j = str_replace('%',$i,$pagelink);

                    if ( ($i != $page) || ((!$more) && ($page==1)) ) {
                        $output .= '<li>';
                        $output .= _wp_link_page($i) ;
                    }
                    else{
                        $output .= '<li class="active">';
                        $output .= _wp_link_page($i) ;
                    }
                    $output .= $link_before . $j . $link_after ;

                    $output .= '</li>';
                }
                $raquo = $page == $numpages ? 'class="disabled"' : '';
                $output .= '<li ' . $raquo .'>' . _wp_link_page($page +1) . '&raquo;</li>';
                $output .= '</ul>' . $after;
            } else {
                if ( $more ) {
                    $output .= $before . '<ul class="pager">';
                    $i = $page - 1;
                    if ( $i && $more ) {
                        $output .= '<li class="previous">' . _wp_link_page($i);
                        $output .= $link_before. $previouspagelink . $link_after . '</li>';
                    }
                    $i = $page + 1;
                    if ( $i <= $numpages && $more ) {
                        $output .= '<li class="next">' .  _wp_link_page($i);
                        $output .= $link_before. $nextpagelink . $link_after . '</li>';
                    }
                    $output .= '</ul>' . $after;
                }
            }
        }

        if ( $echo ){
            echo $output;
        } else {
            return $output;
        }
    }
}
