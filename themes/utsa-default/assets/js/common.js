$(document).ready(function(){
  $('#backToTop').click(function(event) {
    event.preventDefault();
    $('html, body').animate({scrollTop: 0}, 500);
    return false;
  });

  /*Search Drop*/
  $('#headerSearchCTA').click(function() {
    $('.headerSearchContainer').slideToggle(300).toggleClass('activeSearch');
  });

  function collapseSearch() {
    if($('.headerSearchContainer').hasClass('activeSearch')) {
      $('.headerSearchContainer').removeClass('activeSearch').slideUp(300);
    }
  }

  $('.whiteContainer, .parallaxBanner').click(function(){
    collapseSearch();
  });

  $('.slides-container').click(function(){
    collapseSearch();
  });
});

$(document).ready(function(){
	$('.programsDropDown').click(function(e){
	    e.stopPropagation();
	    $(this).children('.programsContent').slideToggle('fast').toggleClass('programActive');
	    if($(this).children('.programsContent').hasClass('programActive'))
	    {
	        $(this).find('i').removeClass('fa-arrow-circle-down').addClass('fa-arrow-circle-up');
	    }
	    else
	    {
	        $(this).find('i').removeClass('fa-arrow-circle-up').addClass('fa-arrow-circle-down');
	    }
	});
});
