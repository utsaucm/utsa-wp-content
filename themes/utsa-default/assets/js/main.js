//load Typekit fonts
try{Typekit.load({ async: true });}catch(e){}
jQuery(function($) {

	//UTSA Search modified to work on template
	/*
  $("#backToTop").click(function(e) {
     	return e.preventDefault(), $("html, body").animate({
          scrollTop: 0
      }, 500), !1
  	});
  $("#headerSearchCTA").click(function() {
  	$(".headerSearchContainer").slideToggle(300).toggleClass("activeSearch")
	});
  */

  var prevHtml = '<button type="button" data-role="none" class="sslick-prev slick-arrow" aria-label="Previous" role="button" style=""></button>';
  var nextHtml = '<button type="button" data-role="none" class="sslick-next slick-arrow" aria-label="Next" role="button" style=""></button>';
  var autoplay = false;
  var slickPlaying = true;
  var manualPause = true;
  var s = $('#slides');
  var t = $('a.thumbnails');
  var p = $('a.pause');
  var hoverSpot = $('.slideRow');
  var control = $('#pausePlayControl');

  s.slick({
    accessibility: true,
    arrows: true,
    autoplay: autoplay,
    autoplaySpeed: 15000, //15 seconds
    prevArrow: prevHtml,
    nextArrow: nextHtml,
    pauseOnHover: false,
    speed: 500,
    cssEase: 'linear',
    adaptiveHeight: true,
    fade: true
  });
  
	//contact form
	var form = $('.contact-form');
	form.submit(function () {
		$this = $(this);
		$.post($(this).attr('action'), function(data) {
			$this.prev().text(data.message).fadeIn().delay(3000).fadeOut();
		},'json');
		return false;
	});

	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});
});
