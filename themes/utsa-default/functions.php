<?php
//Defined Textdomain
define('UTSATEXTDOMAIN', wp_get_theme()->get( 'TextDomain' ));

// registaring menu
register_nav_menus( array(
  'primary'   => __('Primary', UTSATEXTDOMAIN),
  'footer'    => __('Footer', UTSATEXTDOMAIN)
));

//Theme Functions
require_once( get_template_directory()  . '/lib/theme-functions.php');
// nav walker
require_once( get_template_directory()  . '/lib/navwalker.php');
require_once( get_template_directory()  . '/lib/mobile-navwalker.php');
require_once( get_template_directory()  . '/lib/template-tags.php');
require_once( get_template_directory()  . '/lib/pagination.php');

add_action('after_setup_theme', function(){
  // post format support
  add_theme_support(
      'post-formats', array('audio', 'gallery', 'image', 'video')
  );
  // post thumbnail support
  add_theme_support('post-thumbnails');
  add_theme_support( 'automatic-feed-links' );
});

if ( is_singular() && get_option( 'thread_comments' ) ){
  wp_enqueue_script( 'comment-reply' );
}

if ( ! function_exists( 'zee_option' ) ) {
  /**
   * Getting theme option
   * @param  boolean $index  [first index of theme array]
   * @param  boolean $index2 [second index of first index array]
   * @return string          [return option data]
   */
  function zee_option($index=false, $index2=false ){
    global $data;
    if( $index2 ){
        return ( isset($data[$index]) and isset($data[$index][$index2]) ) ?  $data[$index][$index2] : '';
    } else {
        return isset( $data[$index] ) ?  $data[$index] : '';
    }
  }
}

// Content width
if ( ! isset( $content_width ) ) {
  $content_width = 600;
}

//  Set title
add_filter( 'wp_title', function( $title, $sep ) {
  global $paged, $page;
  if ( is_feed() ){
    return $title;
  }
  $title .= get_bloginfo( 'name' );

  // Add the site description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description and ( is_home() or is_front_page() ) ){
      $title = "$title $sep $site_description";
  }
  // Add a page number if necessary.
  if ( $paged >= 2 || $page >= 2 ){
      $title = "$title $sep " . sprintf( __( 'Page %s', UTSATEXTDOMAIN ), max( $paged, $page ) );
  }
  return $title;
}, 10, 2 );

// add shortcode tinymce button
add_filter('mce_buttons', function ($mce_buttons) {
  $pos = array_search('wp_more', $mce_buttons, true);
  if ($pos !== false) {
    $buttons = array_slice($mce_buttons, 0, $pos + 1);
    $buttons[] = 'wp_page';
    $mce_buttons = array_merge($buttons, array_slice($mce_buttons, $pos + 1));
  }
  return $mce_buttons;
});

/**
 * Getting post thumbnail url
 * @param  [int]                $pots_ID [Post ID]
 * @return [string]             [Return thumbail source url]
 */
function zee_get_thumb_url($pots_ID) {
  return wp_get_attachment_url( get_post_thumbnail_id( $pots_ID ) );
}

if( ! function_exists('utsamu_scripts') ){
  function utsamu_scripts() {
    // Javascripts
    wp_enqueue_script('bootstrap-utsa', get_template_directory_uri() . '/assets/js/global-wordpress-092017.min.js');
    wp_enqueue_script('main-js',        get_template_directory_uri() . '/assets/js/main.js');
    //wp_enqueue_script('common-js',        get_template_directory_uri() . '/assets/js/common.js');

    // Stylesheet
    wp_enqueue_style('bootstrap-utsa',  get_template_directory_uri() . '/assets/css/global-wordpress-092017.min.css');
    //wp_enqueue_style('animate',         get_template_directory_uri() . '/assets/css/animate.css');
    // Inline css
    //wp_add_inline_style( 'style',       zee_style_options() );
    wp_enqueue_style('style',           get_template_directory_uri() . '/style.css');
    wp_enqueue_style('site-nav',           get_template_directory_uri() . '/assets/css/site-nav.css');
  }
  add_action('wp_enqueue_scripts', 'utsamu_scripts');
}

function disable_wp_emojicons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

// register sidebars
register_sidebar(array(
  'name' => __( 'Sidebar', UTSATEXTDOMAIN ),
  'id' => 'sidebar',
  'description' => __( 'Widgets in this area will be shown on right side.', UTSATEXTDOMAIN ),
  'before_title' => '<h3>',
  'after_title' => '</h3>',
  'before_widget' => '<div id="sidebar" class="col-md-3 sidebar" role="complementary">',
  'after_widget' => '</div>'
  )
);

register_sidebar(array(
  'name' => __( 'Bottom', UTSATEXTDOMAIN ),
  'id' => 'bottom',
  'description' => __( 'Widgets in this area will be shown before Footer.' , UTSATEXTDOMAIN),
  'before_title' => '<h3>',
  'after_title' => '</h3>',
  'before_widget' => '<div class="col-sm-3 col-xs-6">',
  'after_widget' => '</div>'
  )
);
