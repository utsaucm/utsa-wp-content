<?php if(!is_page()) { ?>
</div><!--/#primary-->
</div><!--/.col-lg-12-->
</div><!--/.row-->
</div><!--/.container.-->
</section><!--/#main-->
<?php } ?>

<?php
  $site_name = utsamu_site_get_option('site_office_name');
  $site_email = utsamu_site_get_option('site_email');
  $site_phone = utsamu_site_get_option('site_phone');
  $site_fax = utsamu_site_get_option('site_fax');
  $site_address1 = utsamu_site_get_option('site_address1');
  $site_address2 = utsamu_site_get_option('site_address2');
  $site_address3 = utsamu_site_get_option('site_address3');
  $site_address4 = utsamu_site_get_option('site_address4');
 ?>

<section id="bottom" class="siteFooter">
  <div class="container"><div class="row">
    <div class="col-md-6">
      <div class="about-widget" itemscope="" itemtype="http://schema.org/Organization">
        <h4 itemprop="name"><?php echo $site_name; ?></h4>
        <ul>
          <?php if ($site_address1 != "") { ?>
          <li itemprop="address">
            <p><i class="fa fa-home"></i>
              <strong>Address:</strong>
              <address>
                <?php if ($site_address1 != "") { echo $site_address1 . '<br/>'; } ?>
                <?php if ($site_address2 != "") { echo $site_address2 . '<br/>'; } ?>
                <?php if ($site_address3 != "") { echo $site_address3 . '<br/>'; } ?>
                <?php if ($site_address4 != "") { echo $site_address4 . '<br/>'; } ?>
              </address>
            </p>
          </li>
          <?php } ?>
          <?php if ($site_phone != "") { ?>
          <li itemprop="telephone">
            <p><i class="fa fa-phone"></i> <strong>Phone:</strong> <?php echo $site_phone; ?></p>
          </li>
          <?php } ?>
          <?php if ($site_fax != "") { ?>
          <li itemprop="facsimile">
            <p><i class="fa fa-phone"></i> <strong>Fax:</strong> <?php echo $site_fax; ?></p>
          </li>
          <?php } ?>
          <?php if ($site_email != "") { ?>
          <li>
            <p><i class="fa fa-envelope-o"></i> <strong>Email:</strong> <a href="mailto:<?php echo $site_email; ?>">
              <?php echo $site_email; ?></a></p>
          </li>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="col-md-5 col-md-offset-3 hidden-xs"></div>
  </div></div>
</section>


<section id="backToTop" class="d-block d-sm-block d-md-none d-lg-none d-xl-none">
  <div class="container"><a href="#">Back to Top</a></div>
</section>
<section id="globalFooterContainer" class="text-xs-center">
  <div class="container">
    <div class="row">
      <div itemscope="" itemtype="http://schema.org/Organization" class="col-md-8 text-md-left">
        <p class="info">&#169; 2017 <span itemprop="name">The University of Texas at San Antonio</span><span class="pipes">&#160;|&#160;</span><span itemprop="address">One UTSA Circle San Antonio, TX 78249</span><span class="pipes">&#160;|&#160;</span>Information <span itemprop="telephone"><a href="tel:210-458-4011" style="color:white;">210-458-4011</a></span></p>
        <nav id="globalFooterNav"><a href="http://alerts.utsa.edu">Campus Alerts</a><span class="pipes"> | </span><a href="http://www.utsa.edu/job.html">Jobs</a><span class="pipes"> | </span><a href="http://www.utsa.edu/policies/reqlinks.html">Required Links</a><span class="pipes"> | </span><a href="http://www.utsa.edu/policies">Policies</a><span class="pipes"> | </span><a href="http://www.utsystem.edu">UT System</a><span class="pipes"> | </span><a href="http://www.utsa.edu/Compliance/Hotline.html">Report Fraud</a></nav>
        <p id="copyInfo"><a href="http://www.utsa.edu/ucm/">Produced by University Communications and Marketing</a></p>
      </div>
      <div class="col-md-4 text-xs-center text-right">
        <nav id="socialNav">
          <a href="https://www.facebook.com/utsa"><i class="fa fa-facebook utsa-social-circle" aria-hidden="true">&#160;</i></a>
          <a href="https://twitter.com/utsa"><i class="fa fa-twitter utsa-social-circle" aria-hidden="true">&#160;</i></a>
          <a href="https://www.youtube.com/user/utsa"><i class="fa fa-youtube-play utsa-social-circle" aria-hidden="true">&#160;</i></a>
          <a href="http://www.linkedin.com/edu/the-university-of-texas-at-san-antonio-19521"><i class="fa fa-linkedin-square utsa-social-circle" aria-hidden="true">&#160;</i></a>
          <a href="https://instagram.com/utsa"><i class="fa fa-instagram utsa-social-circle" aria-hidden="true">&#160;</i></a>
        </nav>
      </div>
    </div>
  </div>
</section>

<?php wp_footer(); ?>

</body>
</html>
