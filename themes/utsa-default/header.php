<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
          <?php wp_title('|', true, 'right'); ?>
        </title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
          <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
            <?php wp_head(); ?>
            <!--[if lt IE 9]>
            <script src="<?php echo get_template_directory_uri() ?>/assets/js/html5shiv.js">
            </script>
            <script src="<?php echo get_template_directory_uri() ?>/assets/js/respond.min.js">
            </script>
            <![endif]-->
          </head>
          <!--/head-->
          <body >
            <div id="global-header">
              <section id="banner">
                <!--start search-->
                <section class="headerSearchContainer">
                  <div class="container">
                    <form action="/search/searchresults.html" id="cse-search-box">
                      <input name="cx" type="hidden" value="000238266656426684962:mzli4pte7ko"/>
                      <input name="cof" type="hidden" value="FORID:11"/>
                      <span style="display: none;">
                        <label for="q">Search</label>
                      </span>
                      <span class="wrapper" style="margin-bottom: 0;">
                        <input class="searchfield" id="q" name="q" onblur="if (this.value == '') {this.value = 'Search UTSA';}" onfocus="if (this.value == 'Search UTSA') {this.value = '';}" size="15" type="text" value="Search UTSA"/>
                      </span>
                    </form>
                  </div>
                </section>
                <!--end search-->
                <!--top navigation icons-->
                <div class="container ">
                  <nav id="icon-bar" class="nav nav-inline text-right float-right float-lg-none justify-content-end">
                    <a aria-label="my.utsa.edu" class="nav-link top-link" data-label="global-header" href="http://my.utsa.edu">
                      <i aria-hidden="true" class="fa fa-globe orangetext">&#160;</i>
                      <span class="linkmobile d-none d-lg-inline d-xl-inline"> myUTSA </span>
                    </a>
                    <a aria-label="UTSA Today" class="nav-link top-link" data-label="global-header" href="https://www.utsa.edu/today">
                      <i aria-hidden="true" class="fa fa-newspaper-o orangetext">&#160;</i>
                      <span class="linkmobile d-none d-lg-inline d-xl-inline"> UTSA Today </span>
                    </a>
                    <a aria-label="Visit UTSA" class="nav-link top-link" data-label="global-header" href="https://www.utsa.edu/visit">
                      <i aria-hidden="true" class="fa fa-map-o orangetext">&#160;</i>
                      <span class="linkmobile d-none d-lg-inline d-xl-inline">Visit </span>
                    </a>
                    <a aria-label="UTSA Directory" class="nav-link top-link" data-label="global-header" href="https://www.utsa.edu/directory">
                      <i aria-hidden="true" class="fa fa-users orangetext">&#160;</i>
                      <span class="linkmobile d-none d-lg-inline d-xl-inline"> Directory </span>
                    </a>
                    <a aria-label="Search UTSA" class="nav-link top-link" data-label="global-header" href="#" id="headerSearchCTA">
                      <i aria-hidden="true" class="fa fa-search orangetext">&#160;</i>
                      <span class="linkmobile d-none d-lg-inline d-xl-inline"> Search </span>
                    </a>
                    <!-- hidden-lg-up float-xs-right -->
                    <button class="navbar-toggler d-inline d-lg-none d-xl-none justify-content-end" data-label="global-header-burger" data-target="#startupNavbar" data-toggle="collapse" style="margin-top: -4px !important;" type="button">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar">
                      </span>
                      <span class="icon-bar">
                      </span>
                      <span class="icon-bar">
                      </span>
                    </button>
                  </nav>
                </div>
                <!--top level navigation-->
                <div class="container">
                  <nav class="navbar navbar-expand-lg navbar-dark">
                    <a class="navbar-brand" data-label="global-header-navbar" href="https://www.utsa.edu">
                      <!--width="320" -->
                      <img alt="UTSA" id="logoFull" class="d-none d-lg-none d-xl-inline"              src="<?php echo get_template_directory_uri() . '/assets/images/'; ?>logo4.png"/>
                      <img alt="UTSA" id="logoSmall" class="logo d-inline d-lg-inline d-xl-none hidden-xl-up hidden-md-down"              src="<?php echo get_template_directory_uri() . '/assets/images/'; ?>logo4_sm.png" width="95"/>
                      <!-- [if IE]>
                      <style>            .logoFull {              width: 320px!important;            }            .logo {              width: 100px;            }            </style>
                      <![endif]-->
                    </a>
                    <div class="collapse navbar-collapse mobileMenuSlider" id="startupNavbar">
                      <ul class="nav navbar-nav mr-auto">
                      </ul>
                      <ul class="nav navbar-nav">
                        <li class="nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="https://www.utsa.edu/about">About</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="https://www.utsa.edu/admissions">Admissions</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="https://www.utsa.edu/academics">Academics</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="http://research.utsa.edu">Research</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="https://www.utsa.edu/campuslife">Campus Life</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="https://www.utsa.edu/community">Community</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="http://goutsa.com">Athletics</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="http://alumni.utsa.edu">Alumni</a>
                        </li>
                        <li class="nav-item bottom-nav-item">
                          <a class="nav-link top-nav-link" data-label="global-header-navbar" href="http://giving.utsa.edu">Giving</a>
                        </li>
                      </ul>
                    </div>
                  </nav>
                </div>

              </section>
            </div>
            <!-- site header -->
            <div id="siteHeader" class="">
              <div class="container">
                <div class="siteName"><a href="<?php echo site_url();?>"><?php echo bloginfo(); ?></a></div>
              </div>
            </div>
            <!--END GOBAL UTSA HEADER ### DO NOT REMOVE ### -->

            <section class="" id="site-navigation" >
              <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <!-- Main navigation starts -->
                <?php
                  if (has_nav_menu('primary')) {
                      wp_nav_menu(array(
                      'theme_location'  => 'primary',
                      'container'       => false,
                      'menu_class'      => '',
                      'menu_id'         => 'departmentalPageNav',
                      'fallback_cb'     => 'wp_page_menu',
                      'walker'          => new wp_bootstrap_navwalker()));
                  }
                ?>
              </div>
            </div>
          </div>
            </section>

            <?php get_template_part('sub', 'title'); ?>
              <?php if (! is_page()) {
                    ?>
              <section id="main" class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-12">
                      <div id="primary" class="content-area">
                        <?php
                } ?>
