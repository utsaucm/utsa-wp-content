<?php

// Breadcrumbs
require_once(get_template_directory(). '/lib/breadcrumbs.php');

// Will add a class based on URI content, class will determine background for title bar else it will not add class and background will be set to default.
$url = $_SERVER["REQUEST_URI"];

$isItCategory1 = strpos($url, 'about');
$isItCategory2 = strpos($url, 'scholarships');
$isItCategory3 = strpos($url, 'cadet-life');
$isItCategory4 = strpos($url, 'faculty-staff');
$isItCategory5 = strpos($url, 'news-alumni');
$isItCategory6 = strpos($url, 'helpful-links');

$myclass= "category1";
if ($isItCategory1!==false)
{
    //url contains 'about'
    $myclass= "category1";
}
if ($isItCategory2!==false)
{
    //url contains 'scholarships'
    $myclass= "category2";
}
if ($isItCategory3!==false)
{
    //url contains 'cadet-life'
    $myclass= "category3";
}
if ($isItCategory4!==false)
{
    //url contains 'faculty-staff'
    $myclass= "category4";
}
if ($isItCategory5!==false)
{
    //url contains 'news-alumni'
    $myclass= "category5";
}
if ($isItCategory6!==false)
{
    //url contains 'helpful-links'
    $myclass= "category6";
}



global $post;
if( is_single() ) {

    $title = zee_option('zee_blog_title');

    $sub_title = zee_option('zee_blog_subtitle');

} elseif ( is_category() ) {

    $title = __("Category", UTSATEXTDOMAIN) . " : " . single_cat_title("", false);

    $sub_title = zee_option('zee_blog_subtitle');

} elseif ( is_archive() ) {
    if (is_day()) {
        $title = __("Daily Archives", UTSATEXTDOMAIN) . " : " . get_the_date();

    } elseif (is_month()) {
        $title = __("Monthly Archives", UTSATEXTDOMAIN) . " : " . get_the_date("F Y");

    } elseif (is_year()) {
        $title = __("Yearly Archives", UTSATEXTDOMAIN) . " : " . get_the_date("Y");

    } else {
        $title = __("Blog Archives", UTSATEXTDOMAIN);

    }

    $sub_title = zee_option('zee_blog_subtitle');

} elseif ( is_tag() ) {
    $title = $return .= __("Tag", UTSATEXTDOMAIN) . " : " . single_tag_title("", false);
} elseif ( is_author() ) {
    $title = __("Author: ", UTSATEXTDOMAIN);
} elseif ( is_search() ) {
    $title = __("Search results for", UTSATEXTDOMAIN) . " : " . get_search_query();
} elseif ( is_tax( 'portfolios' ) ) {
    $title = __("Portfolio", UTSATEXTDOMAIN);
} elseif ( is_home() and !is_front_page() ) {

    $page = get_queried_object();

    if( is_null( $page ) ){
        $title = zee_option('zee_blog_title');
        $sub_title = zee_option('zee_blog_subtitle');
    } else {

     $ID = $page->ID;
     $title = $page->post_title;
     $sub_title = get_post_meta($ID, 'page_subtitle', true);
 }


} elseif ( (is_page()) && (!is_front_page()) ) {
    $page = get_queried_object();

    $ID = $page->ID;

    $title = $page->post_title;
    $sub_title = get_post_meta($ID, 'page_subtitle', true);

} elseif( is_front_page() ){

    unset($title);
}
echo (isset($title) ? '

    <section id="title" class="banner '.$myclass.'">
    <div class="overlay" >
    <div class="container">
    <div class="row pt-3 pb-1">
    <div class="col-sm-12">
    <h1>'.$title.'</h1>
    <p>'.$sub_title.'</p>
    </div>
    </div>
    </div>
    </div>
    </section>

    ' : '');
