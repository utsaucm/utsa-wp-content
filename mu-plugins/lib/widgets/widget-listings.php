<?php
/**
 * Listings Widget
 *
 * @package Luxury_League_2016
 * @since 1.0.0
 * @link https://bitbucket.org/lehgarza/Luxury_League_2016
 * @author John David Garza <john.david@lehgarza.com>
 * @copyright Copyright (c) 2016, LehGarza LLC
 * @license http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */
class lg_Listings_Widget extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'listings-widget', 'description' => 'Displays your latest listings.' );
		parent::__construct( 'listings-widget', 'Listings Widget', $widget_ops);
	}

	/*
	function __construct() {
		parent::__construct(
			'listings-widget', // Base ID
			'Listings Widget', // Name
			array('description' => __( 'Displays your latest listings. Outputs the post thumbnail, title and date per listing'))
		   );
	}
	*/

	function widget($args, $instance) {
		extract( $args );
		$title = apply_filters('widget_title', $instance['title']);
		$numberOfListings = $instance['numberOfListings'];
		echo $before_widget;
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		$this->getListings($numberOfListings);
		echo $after_widget;
	}

	function getListings($numberOfListings) {
		//html
		global $post;
		add_image_size( 'listing_widget_size', 150, 150, false );
		$listings = new WP_Query();
		$args = array(
			'post_type' => 'lg_listing',
			'orderby' => 'rand',
			'posts_per_page' => $numberOfListings
		);
		$listings->query( $args );
		if($listings->found_posts > 0) {
			echo '<ul class="listings-widget">';
				while ($listings->have_posts()) {
					$listings->the_post();
					$post_prefix = '_lg_';
					$author_id = $post->post_author;

					$images = get_post_meta( $post->ID, $post_prefix . 'images', true );
					$image = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'listing_widget_size') : '<div class="noThumb"></div>';
					$listItem = '<li class="recent-post-list">';
					//$listItem .= print_r($post);
					$listItem .= '<a href="' . get_permalink() . '">';
					// . $author_id . ' ';
					//$listItem .= the_author();

					$listItem .= '<span class="recent-post-image">';
					$listItem .= $image;
				  $listItem .= '</span>';
					$listItem .= '<div class="recent-post-title">';
					$listItem .= '<p>' . get_the_title() . '</p>';
					$listItem .= '<div class="posted-on">Listed by ' . bp_core_get_userlink($author_id, false) . '</div>';
					$listItem .= '</div></a></li>';
					echo $listItem;
				}
			echo '</ul>';
			wp_reset_postdata();
		}else{
			echo '<p style="padding:25px;">No listings found</p>';
		}
	}

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['numberOfListings'] = strip_tags($new_instance['numberOfListings']);
		return $instance;
	}

	function form($instance) {
		if( $instance) {
			$title = esc_attr($instance['title']);
			$numberOfListings = esc_attr($instance['numberOfListings']);
		} else {
			$title = '';
			$numberOfListings = '';
		}

		echo '<p><label for="' . $this->get_field_id('title') . '">' . _e('Title', 'listings-widget') . '</label>';
		echo '<input class="widefat" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
		echo '</p><p>';
		echo '<label for="' . $this->get_field_id('numberOfListings') . '">' . _e('Number of Listings:', 'listings-widget') . '</label>';
		echo '<select id="' . $this->get_field_id('numberOfListings') . '" name="' . $this->get_field_name('numberOfListings') .  '">';
		for($x=1;$x<=10;$x++) {
			echo '<option ';
			if ($x == $numberOfListings) {
				echo 'selected="selected"';
			}
			echo ' value="' . $x . '">' . $x . '</option>';
		}
		echo '</select></p>';
	}

}

function lg_register_listings_widget() {
	register_widget('lg_Listings_Widget');
}
add_action( 'widgets_init', 'lg_register_listings_widget' );
