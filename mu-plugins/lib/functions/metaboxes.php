<?php
/**
 * Metaboxes
 *
 * This file registers any custom metaboxes
 *
 * @package      Core_Functionality
 * @since        1.0.0
 * @link         https://github.com/billerickson/Core-Functionality
 * @author       John David Garza <john.garza@utsa.edu>
 * @author       Bill Erickson <bill@billerickson.net>
 * @copyright    Copyright (c) 2015, Leh Garza
 * Modified: 01/2013 Original work by Bill Erickson (https://github.com/billerickson/Core-Functionality)
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

require_once WPMU_PLUGIN_DIR . '/lib/cmb2/init.php';
require_once WPMU_PLUGIN_DIR . '/lib/cmb2-attached-posts/cmb2-attached-posts-field.php';

/**
 * Create Metaboxes
 * @since 1.0.0
 * @link http://www.billerickson.net/wordpress-metaboxes/
 */
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */

function utsamu_slider_metaboxes( array $meta_boxes ) {
	$utsamu_prefix = MU_PREFIX . 'slider_';
	$postTypeTarget = MU_PREFIX . 'slider';

	$meta_boxes['utsamu_slider_details'] = array (
		'id'            => $utsamu_prefix . 'details-area',
		'title'         => __( 'Slider Details', 'cmb2' ),
		'object_types'  => array( $postTypeTarget, ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		'fields'        => array(
			array(
				'name'    => __( 'Background Image', 'cmb2' ),
				'desc'    => __( 'Show background image in slider', 'cmb2' ),
				'id'      => $utsamu_prefix . 'background_image',
				'type'    => 'file',
			),

			array(
				'name'    => __( 'Button Text', 'cmb2' ),
				'desc'    => __( 'Show Slider Button and Button Text"', 'cmb2' ),
				'id'      => $utsamu_prefix . 'button_text',
				'type'    => 'text',
			),

			array(
				'name'    => __( 'Button URL', 'cmb2' ),
				'desc'    => __( 'Slider URL link"', 'cmb2' ),
				'id'      => $utsamu_prefix . 'button_url',
				'type'    => 'text',
			),

			array(
				'name'             => 'Boxed Style',
				'desc'             => 'Show boxed Style',
				'id'               => $utsamu_prefix . 'boxed',
				'type'             => 'select',
				'show_option_none' => false,
				'default'          => 'No',
				'options'          => array(
					'no' => __( 'No', 'cmb2' ),
					'yes'   => __( 'Yes', 'cmb2' ),
					),
			),

			array(
				'name'             => 'Position',
				'desc'             => 'Show slider Position',
				'id'               => $utsamu_prefix . 'position',
				'type'             => 'select',
				'show_option_none' => false,
				'default'          => 'left',
				'options'          => array(
					'left' => __( 'Left', 'cmb2' ),
					'center' => __( 'Center', 'cmb2' ),
					'right'   => __( 'Right', 'cmb2' ),
				),
			),
		)
	);

	$meta_boxes['utsamu_video_details'] = array (
		'id'            => $utsamu_prefix . 'video-area',
		'title'         => __( 'Video Details', 'cmb2' ),
		'object_types'  => array( $postTypeTarget, ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		'fields'        => array(
			array(
				'name'             => 'Video Type',
				'desc'             => 'Select video type',
				'id'               => $utsamu_prefix . 'video_type',
				'type'             => 'radio',
				'show_option_none' => false,
				'options'          => array(
					'none' => __( 'None', 'cmb2' ),
					'youtube' => __( 'youtube', 'cmb2' ),
					'vimeo'   => __( 'vimeo', 'cmb2' ),
					),
			),

			array(
				'name'    => __( 'Video Link', 'cmb2' ),
				'desc'    => __( 'Video link', 'cmb2' ),
				'id'      => $utsamu_prefix . 'video_link',
				'type'    => 'text',
			),
		)
	);

	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'utsamu_slider_metaboxes' );

function utsamu_page_metabox( array $meta_boxes ) {
	$utsamu_prefix = MU_PREFIX . 'page_';

	$meta_boxes['utsamu_subtitle_details'] = array (
		'id'            => $utsamu_prefix . 'subtitle-area',
		'title'         => __( 'Subtitle Details', 'cmb2' ),
		'object_types'  => array( 'page', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		'fields'        => array(
			array(
				'name'    => __( 'Subtitle', 'cmb2' ),
				'id'      => $utsamu_prefix . 'subtitle',
				'type'    => 'text',
			),
		),
	);
	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'utsamu_page_metabox' );

if (!function_exists("utsamu_display_meta_value")) {
	function utsamu_display_meta_value( $id, $uid ) {
		$uid = MU_PREFIX . $uid;
		$text = get_post_meta( $id, $uid, true );
		echo esc_html( $text );
	}
}

if (!function_exists("utsamu_get_meta_value")) {
	function utsamu_get_meta_value( $id, $uid ) {
		$uid = MU_PREFIX . $uid;
		$text = get_post_meta( $id, $uid, true );
		$text =  esc_html( $text );
		return $text;
	}
}
