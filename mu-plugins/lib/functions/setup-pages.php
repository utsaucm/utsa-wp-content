<?php
/**
 * Setup Pages
 *
 * This file registers any custom post types
 *
 * @package      Core_Functionality
 * @since        1.0.0
 * @link         https://github.com/billerickson/Core-Functionality
 * @author       John David Garza <john.garza@utsa.edu>
 * @author       Bill Erickson <bill@billerickson.net>
 * @copyright    Copyright (c) 2015, Leh Garza
 * Modified: 01/2013 Original work by Bill Erickson (https://github.com/billerickson/Core-Functionality)
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

function utsamu_setup_primary_nav() {
	$primaryMenuName = 'utsamu_' . 'Example Top Menu';
  $menuExists = wp_get_nav_menu_object( $primaryMenuName );
  if ( !$menuExists ) {
    error_log('attempt to create menu items');
  }
  /*

// If it doesn't exist, let's create it.
if( !$menu_exists){
    $menu_id = wp_create_nav_menu($menuname);

    // Set up default BuddyPress links and add them to the menu.
    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' =>  __('Home'),
        'menu-item-classes' => 'home',
        'menu-item-url' => home_url( '/' ),
        'menu-item-status' => 'publish'));

  //wp_update_nav_menu_item($menu_id, 0, array('menu-item-title' => 'About',
                                           'menu-item-object' => 'page',
                                           'menu-item-object-id' => get_page_by_path('about')->ID,
                                           'menu-item-type' => 'post_type',
                                           'menu-item-status' => 'publish'));
                                           */
}
function utsamu_setup_required_pages() {
	global $user_ID;
	$new_post = array(
	'post_title' => 'My New Post',
	'post_content' => 'Lorem ipsum dolor sit amet...',
	'post_status' => 'publish',
	'post_date' => date('Y-m-d H:i:s'),
	'post_author' => $user_ID,
	'post_type' => 'post',
	'post_category' => array(0)
	);
	$post_id = wp_insert_post($new_post);
}

//add_action( 'init', 'utsamu_register_required_pages' );
//add_action( 'init', 'utsamu_setup_primary_nav' );
