<?php
/**
 * Post Types
 *
 * This file registers any custom post types
 *
 * @package      Core_Functionality
 * @since        1.0.0
 * @link         https://github.com/billerickson/Core-Functionality
 * @author       John David Garza <john.garza@utsa.edu>
 * @author       Bill Erickson <bill@billerickson.net>
 * @copyright    Copyright (c) 2015, Leh Garza
 * Modified: 01/2013 Original work by Bill Erickson (https://github.com/billerickson/Core-Functionality)
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */
/*
function utsamu_register_example_post_type() {
	$labels = array(
		'name' => 'Example',
		'singular_name' => 'Example',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Example',
		'edit_item' => 'Edit Example',
		'new_item' => 'New Example',
		'view_item' => 'View Example',
		'search_items' => 'Search Example',
		'not_found' =>  'No Example found',
		'not_found_in_trash' => 'No Examples found in trash',
		'parent_item_colon' => '',
		'menu_name' => 'Examples'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => array('utsamu_example', 'utsamu_example'),
		'map_meta_cap' => true,
		'has_archive' => false,
		'hierarchical' => true, //must be true to support menu order
		'menu_position' => null,
		'menu_icon' => 'dashicons-admin-multisite',
		'supports' => array('title', 'author','thumbnail','page-attributes')
	);
	register_post_type( 'utsamu_example', $args );
}
*/

//add_action( 'init', 'utsamu_register_example_post_type' );

function utsamu_register_slider_post_type() {
	$labels = array(
		'name' => 'Slider',
		'singular_name' => 'Slider',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Slider',
		'all_items' => 'All Sliders',
		'edit_item' => 'Edit Slider',
		'new_item' => 'New Slider',
		'view_item' => 'View Slider',
		'search_items' => 'Search Slider',
		'not_found' =>  'No Slider found',
		'not_found_in_trash' => 'No sliders found in trash',
		'parent_item_colon' => '',
		'menu_name' => 'Sliders'
	);

  $args = array(
    'labels'                => $labels,
    'public'                => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'menu_icon'             => 'dashicons-images-alt2',
    'rewrite'               => true,
    'capability_type'       => 'post',
    'supports'              => array('title', 'page-attributes', 'editor', 'thumbnail')
  );
	$ptName = MU_PREFIX . 'slider';
  register_post_type($ptName, $args);
  flush_rewrite_rules();
}

add_action( 'init', 'utsamu_register_slider_post_type' );
