<?php
/**
 * General
 *
 * This file contains any general functions
 *
 * @package      Core_Functionality
 * @since        1.0.0
 * @link         https://github.com/billerickson/Core-Functionality
 * @author       John David Garza <john.garza@utsa.edu>
 * @author       Bill Erickson <bill@billerickson.net>
 * @copyright    Copyright (c) 2015, Leh Garza
 * Modified: 01/2013 Original work by Bill Erickson (https://github.com/billerickson/Core-Functionality)
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

/**
 * Don't Update Plugin
 * @since 1.0.0
 *
 * This prevents you being prompted to update if there's a public plugin
 * with the same name.
 *
 * @author Mark Jaquith
 * @link http://markjaquith.wordpress.com/2009/12/14/excluding-your-plugin-or-theme-from-update-checks/
 *
 * @param array $r, request arguments
 * @param string $url, request url
 * @return array request arguments
 */
function utsamu_core_functionality_hidden( $r, $url ) {
	if ( 0 !== strpos( $url, 'http://api.wordpress.org/plugins/update-check' ) )
		return $r; // Not a plugin update request. Bail immediately.
	$plugins = unserialize( $r['body']['plugins'] );
	unset( $plugins->plugins[ plugin_basename( __FILE__ ) ] );
	unset( $plugins->active[ array_search( plugin_basename( __FILE__ ), $plugins->active ) ] );
	$r['body']['plugins'] = serialize( $plugins );
	return $r;
}
//add_filter( 'http_request_args', 'utsamu_core_functionality_hidden', 5, 2 );

// Use shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );


/**
 * Remove Menu Items
 * @since 1.0.0
 *
 * Remove unused menu items by adding them to the array.
 * See the commented list of menu items for reference.
 *
 */
function utsamu_remove_menus () {
	global $menu;
	$restricted = array(__('Links'));
	// Example:
	//$restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));
	end ($menu);
	while (prev($menu)){
		$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
	}
}
//add_action( 'admin_menu', 'utsamu_remove_menus' );

/**
 * Customize Admin Bar Items
 * @since 1.0.0
 * @link http://wp-snippets.com/addremove-wp-admin-bar-links/
 */
function utsamu_admin_bar_items() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'new-link', 'new-content' );
}
//add_action( 'wp_before_admin_bar_render', 'utsamu_admin_bar_items' );


/**
 * Customize Menu Order
 * @since 1.0.0
 *
 * @param array $menu_ord. Current order.
 * @return array $menu_ord. New order.
 *
 */
function utsamu_custom_menu_order( $menu_ord ) {
	if ( !$menu_ord ) return true;
	return array(
		'index.php', // this represents the dashboard link
		'edit.php?post_type=page', //the page tab
		'edit.php', //the posts tab
		'edit-comments.php', // the comments tab
		'upload.php', // the media manager
    );
}
//add_filter( 'custom_menu_order', 'utsamu_custom_menu_order' );
//add_filter( 'menu_order', 'utsamu_custom_menu_order' );

/**
 * Pretty Printing
 *
 * @author Chris Bratlien
 *
 * @param mixed
 * @return null
 */
function utsamu_pp( $obj, $label = '' ) {

	$data = json_encode(print_r($obj,true));
    ?>
    <style type="text/css">
      #bsdLogger {
      position: absolute;
      top: 30px;
      right: 0px;
      border-left: 4px solid #bbb;
      padding: 6px;
      background: white;
      color: #444;
      z-index: 999;
      font-size: 1.25em;
      width: 400px;
      height: 800px;
      overflow: scroll;
      }
    </style>
    <script type="text/javascript">
      var doStuff = function(){
        var obj = <?php echo $data; ?>;
        var logger = document.getElementById('bsdLogger');
        if (!logger) {
          logger = document.createElement('div');
          logger.id = 'bsdLogger';
          document.body.appendChild(logger);
        }
        ////console.log(obj);
        var pre = document.createElement('pre');
        var h2 = document.createElement('h2');
        pre.innerHTML = obj;

        h2.innerHTML = '<?php echo addslashes($label); ?>';
        logger.appendChild(h2);
        logger.appendChild(pre);
      };
      window.addEventListener ("DOMContentLoaded", doStuff, false);

    </script>
    <?php
}

/**
 * Disable WPSEO Nag on Dev Server
 *
 */
function utsamu_disable_wpseo_nag( $options ) {
	if( strpos( site_url(), 'localhost' ) || strpos( site_url() ,'master-wp' ) )
		$options['ignore_blog_public_warning'] = 'ignore';
	return $options;
}
//add_filter( 'option_wpseo', 'utsamu_disable_wpseo_nag' );
// Disable WPSEO columns on edit screen
//add_filter( 'wpseo_use_page_analysis', '__return_false' );


function utsamu_remove_admin_bar() {
	$current_user = wp_get_current_user();
	if ( 0 == $current_user->ID ) {
		if (!current_user_can('administrator') && !is_admin()) {
			show_admin_bar(false);
		}
	} else {
		show_admin_bar(false);
	}
}
//add_action('init', 'utsamu_remove_admin_bar');

function utsamu_login_fail_redirect( $username ) {
    $referrer = wp_get_referer();
    if ( $referrer && ! strstr($referrer, 'wp-login') && ! strstr($referrer,'wp-admin') ) {
        wp_redirect( add_query_arg('login', 'failed', $referrer) );
        exit;
    }
}
//add_action( 'wp_login_failed', 'utsamu_login_fail_redirect' );

// function to display widget
function utsamu_example_dashboard_widget() {
	global $post, $current_user;
	$utsamu_prefix = '_utsamu_';
  //define arguments for WP_Query()
  $qargs = array(
		'post_type' => 'utsamu_example',
		'order' => 'DESC',
		'orderby' => 'modified',
		'posts_per_page' => 5
  );
  // perform the query
  $q = new WP_Query();
  $q->query($qargs);
	//error_log(print_r($qargs, true));

  // setup the content with a list
	echo '<ul>';
	// execute the WP loop
  while ($q->have_posts()) : $q->the_post();
		echo '<li>';
		echo '<a href="'. get_edit_post_link() .'" rel="bookmark">';
		if (has_post_thumbnail()) {
			the_post_thumbnail('thumbnail');
		}
		echo ' ' . get_the_title() .'  (';
 		echo ')</a>';
		echo '</li>';
  endwhile;
	echo '</ul>';
}

//function to setup widget
function add_dashboard_widgets() {
  // create a dashboard widget called "private_page_menu_dashboard_widget" with the title "Private Pages Menu" and call our display function to draw it
  wp_add_dashboard_widget('private_page_menu_dashboard_widget', 'Example Dashboard Widget', 'utsamu_example_dashboard_widget' );
}

// finally we have to hook our function into the dashboard setup using add_action
add_action('wp_dashboard_setup', 'add_dashboard_widgets');

function utsamu_tidy_dashboard() {
  global $wp_meta_boxes, $current_user;
  // remove incoming links info for authors or editors
  if(in_array('utsamu_example_role', $current_user->roles)) {
    unset($wp_meta_boxes['dashboard']['normal ']['core']['dashboard_incoming_links']);
		// remove the plugins info and news feeds for everyone
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		//Right Now - Comments, Posts, Pages at a glance
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		//Recent Comments
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		//Incoming Links
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		//Plugins - Popular, New and Recently updated Wordpress Plugins
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		//Wordpress Development Blog Feed
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		//Other Wordpress News Feed
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		//Quick Press Form
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		//Recent Drafts List
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
  }
}
//add our function to the dashboard setup hook
add_action('wp_dashboard_setup', 'utsamu_tidy_dashboard');

//add_filter('parse_query', 'utsamu_author_media_only' );
function utsamu_remove_admin_bar_links() {
	global $wp_admin_bar, $current_user;
	if(in_array('utsamu_example_role', $current_user->roles)) {
		$wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
		$wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
		$wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
		//$wp_admin_bar->remove_menu('site-name');        // Remove the site name menu
		$wp_admin_bar->remove_menu('view-site');        // Remove the view site link
		$wp_admin_bar->remove_menu('updates');          // Remove the updates link
		$wp_admin_bar->remove_menu('new-content');      // Remove the content link
		//$wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
		//$wp_admin_bar->remove_menu('my-account');       // Remove the user details tab
	}
	$wp_admin_bar->remove_menu('edit-profile');         // Remove the comments link
	$wp_admin_bar->remove_menu('user-info');         // Remove the comments link
	$wp_admin_bar->remove_menu('bp-notifications');         // Remove the comments link
	$wp_admin_bar->remove_menu('my-account-buddypress');         // Remove the comments link
	$wp_admin_bar->remove_menu('my-account-xprofile');         // Remove the comments link
	$wp_admin_bar->remove_menu('my-account-notifications');         // Remove the comments link
	$wp_admin_bar->remove_menu('my-account-settings');         // Remove the comments link
	$wp_admin_bar->remove_menu('my-account-activity');         // Remove the comments link
	$wp_admin_bar->remove_menu('comments');         // Remove the comments link
	$wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
	$wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
	$wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
}
add_action( 'wp_before_admin_bar_render', 'utsamu_remove_admin_bar_links' );
