<?php
	
// include js
add_action('admin_enqueue_scripts', function($page){

  // check if this your page here with the upload form!
	error_log( get_post_type() );
  if(($page !== 'post.php') || (get_post_type() !== 'common_asset'))
    return;

  wp_enqueue_script('plupload-all');
});



// this adds a simple metabox with the upload form on the edit-post page
add_action('add_meta_boxes', function(){
	$meta_args = array ( 'type' => 'common' );
  add_meta_box('gallery_photos', __('Photos'), 'utsamu_upload_meta_box', 'common_asset', 'normal', 'high', $meta_args);

});

function utsamu_common_asset_admin_display( $post ) {
	$asset = get_post_meta( $post->ID, 'utsamu_common_asset', false)[0];
	if (!empty($asset)) {
		$astr = print_r( $asset, true );
		error_log( 'utsamu_common_asset_admin_display: ' . $astr );
		$pathinfo = pathinfo ( $asset['thumbURL'] );
		$pistr = print_r( $pathinfo, true );
		error_log( 'utsamu_common_asset_admin_display: ' . $pistr );
	?>
	<div>
		<?php echo wp_get_attachment_link( $asset['attach_id'], array(150, null), false, false, false ); ?>
	</div>
	<span><?php echo $pathinfo['basename']; ?></span>
	<b></b>
<?php
	}
}

// so here's the actual uploader
// most of the code comes from media.php and handlers.js
function utsamu_upload_meta_box( $post, $meta_args ){ 	?>
<div id="filelist">
	<?php utsamu_common_asset_admin_display( $post ); ?>
</div>

	 <input type="hidden" id="common_asset" value="" />
   <div id="plupload-upload-ui" class="hide-if-no-js">
     <div id="drag-drop-area">
       <div class="drag-drop-inside">
        <p class="drag-drop-info"><?php _e('Drop files here'); ?></p>
        <p><?php _ex('or', 'Uploader: Drop files here - or - Select Files'); ?></p>
        <p class="drag-drop-buttons"><input id="plupload-browse-button" type="button" value="<?php esc_attr_e('Select Files'); ?>" class="button" /></p>
      </div>
     </div>
  </div>

  <?php

  $plupload_init = array(
    'runtimes'            => 'html4',
    'browse_button'       => 'plupload-browse-button',
    'container'           => 'plupload-upload-ui',
    'drop_element'        => 'drag-drop-area',
    'file_data_name'      => 'async-upload',
    'multiple_queues'     => false,
    'max_file_size'       => wp_max_upload_size().'b',
    'url'                 => admin_url('admin-ajax.php'),
    'flash_swf_url'       => includes_url('js/plupload/plupload.flash.swf'),
    'silverlight_xap_url' => includes_url('js/plupload/plupload.silverlight.xap'),
    'filters'             => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
    'multipart'           => true,
    'urlstream_upload'    => true,

    // additional post data to send to our ajax hook
    'multipart_params'    => array(
      '_ajax_nonce' => wp_create_nonce('photo-upload'),
      'action'      => 'photo_gallery_upload',
			'post_id'			=> $post->ID,
    ),
  );

  // we should probably not apply this filter, plugins may expect wp's media uploader...
  $plupload_init = apply_filters('plupload_init', $plupload_init); ?>

  <script type="text/javascript">

    jQuery(document).ready(function($){
      var $this = $(this);
			var $filelist = $(this).find('#filelist');

      // create the uploader and pass the config from above
      var uploader = new plupload.Uploader(<?php echo json_encode($plupload_init); ?>);

      // checks if browser supports drag and drop upload, makes some css adjustments if necessary
      uploader.bind('Init', function(up){
        var uploaddiv = $('#plupload-upload-ui');

        if(up.features.dragdrop){
          uploaddiv.addClass('drag-drop');
            $('#drag-drop-area')
              .bind('dragover.wp-uploader', function(){ uploaddiv.addClass('drag-over'); })
              .bind('dragleave.wp-uploader, drop.wp-uploader', function(){ uploaddiv.removeClass('drag-over'); });

        }else{
          uploaddiv.removeClass('drag-drop');
          $('#drag-drop-area').unbind('.wp-uploader');
        }
      });

      uploader.init();

      // a file was added in the queue
      uploader.bind('FilesAdded', function(up, files){
        var hundredmb = 100 * 1024 * 1024, max = parseInt(up.settings.max_file_size, 10);

        plupload.each(files, function(file){
          if (max > hundredmb && file.size > hundredmb && up.runtime != 'html5'){
            // file size error?

          }else{
            // a file was added, you may want to update your DOM here...
            console.log(file);
            plupload.each(files, function(file) {
							$filelist.html('');
							$filelist.html('<span>' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b>');
            });
          }
        });

        up.refresh();
        up.start();
      });

			uploader.bind('UploadProgress', function(up, file) {
        document.getElementById('filelist').getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
			});

      // a file was uploaded
      uploader.bind('FileUploaded', function(up, file, response) {
        // this is your ajax response, update the DOM with it or something...
        console.log(response['response']);
				var newImg = '<div><img src="' + response['response'] + '" /></div>';
				console.log(file.id);
				$filelist.prepend(newImg);
				//$this.find('#plupload-upload-ui').hide();
      });

    });

  </script>
  <?php
}

function utsamu_asset_common_upload_dir( $upload ) {
  $upload['subdir'] = '/toolbox/common';
  $upload['path']   = $upload['basedir'] . $upload['subdir'];
  $upload['url']    = $upload['baseurl'] . $upload['subdir'];
	//$ustr = print_r( $upload, true );
	//error_log($ustr);
  return $upload;
}

// handle uploaded file here
add_action('wp_ajax_photo_gallery_upload', 'utsamu_ajax_file_upload');

function utsamu_common_asset_update( $post_id, $file_info ) {
	$asset = get_post_meta( $post_id, 'utsamu_common_asset', false);
	if (!empty($asset)) {

	} else {

	}
	delete_post_meta($post_id, 'utsamu_common_asset');
  add_post_meta($post_id, 'utsamu_common_asset', $file_info);
	$upstr = print_r( $file_info, true );
	error_log('FILE_INFO DETAILS: ' . $upstr );
}


function utsamu_common_asset_create_attachment( $post, $file_info ) {
	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $file_info['url'],
		'post_mime_type' => $file_info['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', $file_info['name'] ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);

	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $filename, $post->ID );
	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata( $attach_id, $file_info['file'] );
	wp_update_attachment_metadata( $attach_id, $attach_data );
	return $attach_id;
}

function utsamu_common_asset_create_thumb ( $post_id, &$file_info, $upload_info ) {
	$image = wp_get_image_editor( $file_info['file'] );
	$pathParts = pathinfo( $file_info['file'] );
	$ppstr = print_r( $pathParts, true );
	error_log("pathparts: " . $ppstr);
	$thumbName = $pathParts['filename'] . '_150.' . $pathParts['extension'];
	$thumbFile = trailingslashit($pathParts['dirname']) . $thumbName;
	$thumbURL = trailingslashit($upload_info['url']) . $thumbName;
	$file_info['thumbURL'] = $thumbURL;
	$file_info['thumbFile'] = $thumbFile;
	$file_info['thumbName'] = $thumbName;
	if ( ! is_wp_error( $image ) ) {
	    $image->resize( 150, null, true );
	    $image->save( $thumbFile );
	} else {
		echo( "error while attempting thumbnail resize" );
	}
}

function utsamu_ajax_file_upload( $post ){
	$pid = $_POST['post_id'];

  check_ajax_referer('photo-upload');

	if ( ! function_exists( 'wp_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}

  $temp_file = $_FILES['async-upload'];
	$upstr = print_r( $_FILES['async-upload'], true );
	error_log('ASYNC DETAILS: ' . $upstr );
	$upload_overrides = array( 'test_form' => false );

	add_filter('upload_dir', 'utsamu_asset_common_upload_dir');
	$upload_info = utsamu_asset_common_upload_dir( wp_upload_dir() );
	$move_file = wp_handle_upload( $temp_file, $upload_overrides, '' );
	remove_filter('upload_dir', 'utsamu_asset_common_upload_dir');

	$move_file['name'] = $temp_file['name'];
	$move_file['size'] = $temp_file['size'];
	$move_file['type'] = $temp_file['type'];
	//utsamu_common_asset_create_thumb( $pid, $move_file, $upload_info );

	$mfstr = print_r( $move_file, true );
	error_log("move_file: " . $mfstr);

	$attach_id = utsamu_common_asset_create_attachment( $post, $move_file );
	$move_file['attach_id'] = $attach_id;
	utsamu_common_asset_update( $pid, $move_file );
	echo $move_file['thumbURL'];
  exit;
};
