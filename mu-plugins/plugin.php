<?php
/**
 * Plugin Name: UTSA Must Use 2017 Core Functionality
 * Plugin URI: http://www.utsa.edu
 * Description: This contains all your site's core functionality so that it is theme independent.
 * Version: 1.0.0 (19-Jun-2017)
 * Author: John David Garza
 *
 * Modified by: John David Garza <john.garza@utsa.edu>
 * Several years later, more iteration, no real substantial changes, upgraded to latest
 * CMB2 (verison 2.0.8) as of 1/24/16.
 *
 * Modified: 2/2015 - Last major modifications made during usage
 *
 * Modified: 01/2013 Original work by Bill Erickson (https://github.com/billerickson/Core-Functionality)
 * More info at author's url: http://www.billerickson.net/core-functionality-plugin/
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

// Plugin Directory
define( 'UTSA_MU_DIR', dirname( __FILE__ ) );
define( 'MU_PREFIX', '_utsamu_');

// Default Required pages
// include_once( UTSA_MU_DIR . '/lib/function/setup-pages.php' );

// Post Types
include_once( UTSA_MU_DIR . '/lib/functions/post-types.php' );

// Taxonomies
//include_once( UTSA_MU_DIR . '/lib/functions/taxonomies.php' );

// Metaboxes - should be using CMB2 now!
include_once( UTSA_MU_DIR . '/lib/functions/metaboxes.php' );

include_once( UTSA_MU_DIR . '/lib/functions/site-options.php' );

// General
include_once( UTSA_MU_DIR . '/lib/functions/general.php' );

// TGM-Plugin-Activations
include_once( UTSA_MU_DIR . '/lib/functions/tgm-plugin-activation.php');

// Widgets
// include_once( UTSA_MU_DIR . '/lib/widgets/widget-listings.php');
